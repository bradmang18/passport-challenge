<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class NumberNode extends Model
{
    protected $fillable = ['value'];

    /**
     * Get the factory relationship.
     *
     * @return BelongsTo
     */
    public function factory()
    {
        return $this->belongsTo(Factory::class);
    }
}