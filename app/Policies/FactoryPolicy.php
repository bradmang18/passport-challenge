<?php
namespace App\Policies;

use App\User;
use App\Factory;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class FactoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the factory.
     *
     * @param User $user
     * @param Factory $factory
     * @return bool
     */
    public function view(User $user, Factory $factory)
    {
        return $user->id === $factory->user->id;
    }

    /**
     * Determine whether the user can create factories.
     *
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return Auth::check();
    }

    /**
     * Determine whether the user can update the factory.
     *
     * @param User $user
     * @param Factory $factory
     * @return bool
     */
    public function update(User $user, Factory $factory)
    {
        return $user->id === $factory->user->id;
    }

    /**
     * Determine whether the user can delete the factory.
     *
     * @param User $user
     * @param Factory $factory
     * @return bool
     */
    public function delete(User $user, Factory $factory)
    {
        return $user->id === $factory->user->id;
    }
}
