<?php
namespace App\Services;

use App\Events\FactoryCreated;
use App\Events\FactoryDeleted;
use App\Events\FactoryUpdated;
use App\Factory;
use App\NumberNode;
use App\User;
use Illuminate\Support\Collection;

class FactoryService
{
    /**
     * Get a collection of factories for the supplied User.
     *
     * @param User $user
     * @return Collection
     */
    public function getFactoriesForUser(User $user)
    {
        return $user->factories()->oldest()->with('numberNodes')->get();
    }

    /**
     * Create a factory and assign it to the supplied User.
     *
     * @param User $user
     * @param string $name
     * @param int $lowerBound
     * @param int $upperBound
     * @param int $childCount
     * @return Factory
     */
    public function createFactoryForUser(User $user, $name, $lowerBound, $upperBound, $childCount)
    {
        $factory = Factory::create(['name' => $name, 'lower_bound' => $lowerBound, 'upper_bound' => $upperBound]);
        $factory->user()->associate($user);
        $factory->numberNodes()->saveMany($this->createNodes($childCount, $lowerBound, $upperBound));
        $factory->save();
        $factory->loadMissing('numberNodes');

        broadcast(new FactoryCreated($factory))->toOthers();

        return $factory;
    }

    /**
     * Update an existing factory with new parameters.
     *
     * @param Factory $factory
     * @param string $name
     * @param int $lowerBound
     * @param int $upperBound
     * @param int $childCount
     * @return Factory
     */
    public function updateFactory(Factory $factory, $name, $lowerBound, $upperBound, $childCount)
    {
        $factory->fill(['name' => $name, 'lower_bound' => $lowerBound, 'upper_bound' => $upperBound]);
        $factory->numberNodes()->delete();
        $factory->numberNodes()->saveMany($this->createNodes($childCount, $lowerBound, $upperBound));
        $factory->save();
        $factory->loadMissing('numberNodes');

        broadcast(new FactoryUpdated($factory))->toOthers();

        return $factory;
    }

    /**
     * Delete the supplied Factory.
     *
     * @param Factory $factory
     */
    public function deleteFactory(Factory $factory)
    {
        broadcast(new FactoryDeleted($factory))->toOthers();

        $factory->delete();
    }

    /**
     * Create an array of NumberNodes based on the supplied parameters.
     *
     * @param int $count
     * @param int $lowerBound
     * @param int $upperBound
     * @return NumberNode[]
     */
    protected function createNodes($count, $lowerBound, $upperBound)
    {
        $nodes = [];
        for ($i = 0; $i < $count; $i++) {
            $nodes[] = (new NumberNode())->fill(['value' => rand($lowerBound, $upperBound)]);
        }
        return $nodes;
    }
}