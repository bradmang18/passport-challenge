<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FactoryRequest extends FormRequest
{
    const MIN_BOUND = 1;
    const MAX_BOUND = 4000000000;
    const MAX_CHILD_NODES = 15;
    const MAX_NAME_LENGTH = 191;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:' . self::MAX_NAME_LENGTH,
            'lower_bound' => 'required|integer|min:' . self::MIN_BOUND . '|max:' . self::MAX_BOUND,
            'upper_bound' => 'required|integer|min:' . self::MIN_BOUND . '|max:' . self::MAX_BOUND,
            'child_count' => 'required|integer|min:1|max:' . self::MAX_CHILD_NODES
        ];
    }

    /**
     * Get the data to use for validation.
     *
     * @return mixed
     */
    protected function getValidationData()
    {
        return $this->json();
    }
}
