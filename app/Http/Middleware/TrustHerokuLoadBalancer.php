<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;

class TrustHerokuLoadBalancer
{
    const SUPPORTED_ENVIRONMENTS = ['production'];

    protected $app;

    /**
     * Create new instance.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (in_array($this->app->environment(), self::SUPPORTED_ENVIRONMENTS)) {
            Request::setTrustedProxies([
                $request->getClientIp()
            ]);
        }

        return $next($request);
    }
}
