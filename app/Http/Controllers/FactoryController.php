<?php
namespace App\Http\Controllers;

use App\Http\Requests\FactoryRequest;
use App\Services\FactoryService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Factory;

class FactoryController extends Controller
{
    protected $factoryService;

    public function __construct(FactoryService $factoryService)
    {
        $this->factoryService = $factoryService;
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return response()->json($this->factoryService->getFactoriesForUser($request->user()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FactoryRequest $request
     * @return Response
     */
    public function store(FactoryRequest $request)
    {
        $this->authorize('create', Factory::class);

        $name = $request->input('name');
        $lowerBound = intval($request->input('lower_bound'));
        $upperBound = intval($request->input('upper_bound'));
        $childCount = intval($request->input('child_count'));

        $factory = $this->factoryService->createFactoryForUser($request->user(), $name, $lowerBound, $upperBound,
            $childCount);

        return response()->json($factory);
    }

    /**
     * Display the specified resource.
     *
     * @param Factory $factory
     * @return Response
     */
    public function show(Factory $factory)
    {
        $this->authorize('view', $factory);

        return response()->json($factory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param FactoryRequest $request
     * @param Factory $factory
     * @return Response
     */
    public function update(FactoryRequest $request, Factory $factory)
    {
        $this->authorize('update', $factory);

        $name = $request->input('name');
        $lowerBound = intval($request->input('lower_bound'));
        $upperBound = intval($request->input('upper_bound'));
        $childCount = intval($request->input('child_count'));

        $factory = $this->factoryService->updateFactory($factory, $name, $lowerBound, $upperBound, $childCount);

        return response()->json($factory);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Factory $factory
     * @return Response
     */
    public function destroy(Factory $factory)
    {
        $this->authorize('delete', $factory);

        $this->factoryService->deleteFactory($factory);

        return response()->json();
    }
}
