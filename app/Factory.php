<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Factory extends Model
{
    protected $table = 'factories';
    protected $fillable = ['name', 'lower_bound', 'upper_bound'];

    /**
     * Get the numberNodes relationship.
     *
     * @return HasMany
     */
    public function numberNodes()
    {
        return $this->hasMany(NumberNode::class);
    }

    /**
     * Get the user relationship.
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}