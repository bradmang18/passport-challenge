import Factory from '../models/Factory';

export default class FactoryService {
    static getFactories() {
        return axios.get('/api/factories')
            .then(function(response) {
                let factories = [];
                for (let i = 0; i < response.data.length; i++) {
                    factories.push(Factory.fromJson(response.data[i]));
                }
                return factories;
            })
        ;
    }

    static getFactory(id) {
        return axios.get('/api/factories/' + id)
            .then(function(response) {
                return Factory.fromJson(response.data);
            })
        ;
    }

    static createFactory(name, lowerBound, upperBound, childCount) {
        return axios.post('/api/factories', {name: name, lower_bound: lowerBound, upper_bound: upperBound,
            child_count: childCount})
            .then(function(response) {
                return Factory.fromJson(response.data);
            })
        ;
    }

    static updateFactory(id, name, lowerBound, upperBound, childCount) {
        return axios.put('/api/factories/' + id, {name: name, lower_bound: lowerBound, upper_bound: upperBound,
            child_count: childCount})
            .then(function(response) {
                return Factory.fromJson(response.data);
            })
        ;
    }

    static deleteFactory(id) {
        return axios.delete('/api/factories/' + id)
            .then(function(response) {
                return response.data;
            })
        ;
    }
}