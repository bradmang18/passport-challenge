import NumberNode from './NumberNode';

export default class Factory {
    constructor(id, name, lowerBound, upperBound, numberNodes) {
        this.id = id;
        this.name = name;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.numberNodes = numberNodes;
        this.childCount = numberNodes.length;
    }

    static fromJson(json) {
        let numberNodes = [];
        for (let i = 0; i < json.number_nodes.length; i++) {
            numberNodes.push(NumberNode.fromJson(json.number_nodes[i]));
        }

        return new Factory(json.id, json.name, json.lower_bound, json.upper_bound, numberNodes);
    }

    static getDefaultInstance() {
        return new Factory(0, '', 1, 100, []);
    }
}