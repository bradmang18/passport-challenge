export default class NumberNode {
    constructor(id, value) {
        this.id = id;
        this.value = value;
    }

    static fromJson(json) {
        return new NumberNode(json.id, json.value);
    }
}