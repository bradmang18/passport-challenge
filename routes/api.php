<?php

Route::resource('factories', 'FactoryController', [
    'only' => ['index', 'store', 'show', 'update', 'destroy']
]);

Route::get('user', 'UserController@index')->name('user');