<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBoundsToFactories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('factories', function(Blueprint $table) {
            $table->integer('lower_bound')->unsigned()->default(1);
            $table->integer('upper_bound')->unsigned()->default(100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('factories', function(Blueprint $table) {
            $table->dropColumn(['lower_bound', 'upper_bound']);
        });
    }
}
